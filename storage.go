package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"os"
	"sync"
)

var mrFlushMutex = &sync.Mutex{}

// Light wrapper to create a better API
type Storage struct {
	List map[string]*MergeRequest
}

func NewStorage() Storage {
	list := make(map[string]*MergeRequest, 0)
	return Storage{List: list}
}

func Load(store *Storage) error {
	// No file found yet,
	if _, err := os.Stat(conf.StorageFile()); os.IsNotExist(err) {
		return nil
	}

	buff, err := ioutil.ReadFile(conf.StorageFile())
	if err != nil {
		return err
	}

	if err = json.Unmarshal(buff, &store.List); err != nil {
		return err
	}

	return nil
}

func (store *Storage) Store(mr MergeRequest) error {
	store.List[mr.IdString] = &mr
	return store.flush()
}

func (store *Storage) Fetch(idString string) *MergeRequest {
	return store.List[idString]
}

func (store *Storage) Delete(mr string) error {
	delete(store.List, mr)
	return store.flush()
}

func (store *Storage) Prune() []byte {
	var pruned []MergeRequest

	for _, mr := range store.List {
		if mr.IsDone() {
			pruned = append(pruned, *mr)
		}
	}

	for _, mr := range pruned {
		quitChans[mr.IdString] <- true
		store.Delete(mr.IdString)
	}

	var message string

	if len(pruned) == 0 {
		message = "No merge requests were pruned"
	} else if len(pruned) == 1 {
		message = "1 merge request was pruned"
	} else {
		message = fmt.Sprintf("%d merge requests were pruned", len(pruned))
	}

	return []byte(message + "\n")
}

func (store *Storage) flush() error {
	mrFlushMutex.Lock()
	defer mrFlushMutex.Unlock()

	buff, err := json.Marshal(store.List)
	if err != nil {
		return err
	}
	return ioutil.WriteFile(conf.StorageFile(), buff, 0644)
}
