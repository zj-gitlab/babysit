package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"os/signal"
	"syscall"
)

const serverPort = ":13083"

var (
	storage   = NewStorage()
	quitChans = make(map[string]chan interface{})
)

func StartServer() error {
	interrupt := make(chan os.Signal, 1)
	signal.Notify(interrupt, os.Interrupt, os.Kill, syscall.SIGTERM)

	listener, err := net.Listen("tcp", serverPort)
	if err != nil {
		return err
	}
	defer listener.Close()

	// set up channel on which to send accepted connections
	listen := make(chan net.Conn, 4)
	go acceptConnection(listener, listen)

	addFromStore()
	for {
		select {
		case conn := <-listen:
			go handleClient(conn)
		case <-interrupt:
			return nil
		}
	}

	return nil
}

func acceptConnection(listener net.Listener, listen chan<- net.Conn) {
	for {
		conn, err := listener.Accept()
		defer conn.Close()

		if err != nil {
			continue
		}

		listen <- conn
	}
}

func handleClient(conn net.Conn) {
	jsonCmd, _, _ := bufio.NewReader(conn).ReadLine()

	var command Command
	json.Unmarshal(jsonCmd, &command)

	switch command.Cmd {
	case "track":
		conn.Write(trackMergeRequest(command))
	case "prune":
		conn.Write(storage.Prune())
	default:
		conn.Write([]byte("error: unknown command.\n"))
	}
}

func addFromStore() {
	Load(&storage)

	for _, mr := range storage.List {
		go func(mergeRequest MergeRequest) {
			mergeRequest.track()
		}(*mr)
	}
}

func trackMergeRequest(cmd Command) []byte {
	id := cmd.Args["id"]

	var message string
	if storage.Fetch(id).ID != 0 {
		message = "Already tracking this merge request."
	} else {
		mr := NewMergeRequest(id)
		go mr.track()

		message = fmt.Sprintf("Tracking new MR: %s.", id)
	}

	return append([]byte(message), byte('\n'))
}
