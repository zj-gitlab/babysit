package main

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/zj/babysit/config"
)

// So its available globally
var conf = config.Config{}

// Will be overriden by ldflags
var version = "dev"

func main() {
	app := setupCliApp()
	app.Run(os.Args)
}

func setupCliApp() *cli.App {
	app := cli.NewApp()
	app.Name = "babysit"
	app.Version = version
	app.Usage = "Stop staring at your pipelines!"

	app.Flags = []cli.Flag{
		cli.StringFlag{
			Name:  "config, c",
			Usage: fmt.Sprintf("Defaults to %s", config.DefaultConfigPath()),
		},
	}

	app.Commands = []cli.Command{
		{
			Name:    "server",
			Aliases: []string{"s"},
			Usage:   "Start the background server to poll for updates",
			Action: func(c *cli.Context) error {
				conf = readConfig(c.String("config"))
				return StartServer()
			},
		},
		{
			Name:    "list",
			Aliases: []string{"ls"},
			Usage:   "List the tracked MRs",
			Action: func(c *cli.Context) error {
				conf = readConfig(c.String("config"))
				list()
				return nil
			},
		},
		{
			Name:    "track",
			Aliases: []string{"t"},
			Usage:   "Track the passed MRs",
			Action: func(c *cli.Context) error {
				track(c.Args()[0])
				return nil
			},
		},
		{
			Name:  "prune",
			Usage: "Prune closed and merged merge requests from the tracking list",
			Action: func(c *cli.Context) error {
				return prune()
			},
		},
	}

	return app
}

func readConfig(path string) config.Config {
	conf, err := config.NewConfig(path)
	if err != nil {
		fmt.Println(err)
		os.Exit(1)
		return conf
	}

	return conf
}
