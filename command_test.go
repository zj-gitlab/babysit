package main

import (
	"testing"
)

func TestToJSON(t *testing.T) {
	cmd := Command{"track", map[string]string{"id": "1"}}

	expected := `{"command":"track","arguments":{"id":"1"}}`

	json, err := cmd.ToJSON()
	if err != nil {
		t.Errorf("Error when creating json: %v", err)

	}

	if json != expected {
		t.Errorf("error creating json, expected %v, got %v", expected, json)
	}
}
