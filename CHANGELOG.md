The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased

## [v0.2.0] 2017-04-08

### Added
- Prune sub command to stop tracking closed or merged merge requests

### Fixed
- HTTP client was shared between goroutines creating race conditions with
verbose output
- Set correct version when executing `babysit -v`

## [v0.1.0] 2017-03-16

### Added
- Store tracked merge requests on disk
- Load previously tracked merge requests from disk
- When listing merge requests, load them from disk

### Changed
- Move away from usage of private token, use access token.
  - Change in the config file, change TOML key `private_token` to `access_token`
- Move config to `.babysit` directory
- Use APIv4 to request data (saves a request to find out the MRs IID)

## [v0.0.0] 2017-03-05

### Added
- Basic server <-> client architecture
- Add `server`, `track`, and `list` commands

[Unreleased]: https://gitlab.com/zj/babysit/compare/v0.2.0...master
[v0.2.0]: https://gitlab.com/zj/babysit/tags/v0.2.0
[v0.1.0]: https://gitlab.com/zj/babysit/tags/v0.1.0
[v0.0.0]: https://gitlab.com/zj/babysit/commit/61417cbd6e7b80c0452a05849f5997c0a1fcf469
