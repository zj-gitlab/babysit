package main

import (
	"io/ioutil"
	"os"
	"testing"

	"gitlab.com/zj/babysit/config"
)

func TestLoadNonExistingStore(t *testing.T) {
	conf := tempConfig(t)
	defer os.RemoveAll(conf.Path())

	store := NewStorage()

	if err := Load(&store); err != nil {
		t.Fatal(err)
	}

	if len(store.List) != 0 {
		t.Errorf("Expected the store to contain no elements, got %d", len(store.List))
	}
}

func TestLoadExistingStore(t *testing.T) {
	conf := tempConfig(t)
	defer os.RemoveAll(conf.Path())

	store := NewStorage()
	mr := MergeRequest{ID: 2}

	if err := store.Store(mr); err != nil {
		t.Fatal(err)
	}

	// Read back what we stored
	if err := Load(&store); err != nil {
		t.Fatal(err)
	}

	if len(store.List) != 1 {
		t.Errorf("Expected 1 element in the store")
	}
}

func TestStoreFirstMR(t *testing.T) {
	conf = tempConfig(t)
	defer os.RemoveAll(conf.Path())

	store := NewStorage()

	mr := MergeRequest{ID: 2}

	if err := store.Store(mr); err != nil {
		t.Error(err)
	}

	if len(store.List) != 1 {
		t.Fatal("Expected 1 element to be stored")
	}
}

func TestPruneEmptyStorage(t *testing.T) {
	store := NewStorage()

	expected := "No merge requests were pruned\n"
	got := string(store.Prune())
	if got != expected {
		t.Errorf("Expected %s, but got %s", expected, got)
	}
}

func TestPruneStorage(t *testing.T) {
	store := NewStorage()

	mr := NewMergeRequest("zj/babysit!2")
	mr.State = "merged"
	store.Store(mr)

	mr2 := NewMergeRequest("zj/babysit!3")
	mr2.State = "opened"
	store.Store(mr2)

	quitChan := make(chan interface{}, 1)
	quitChan2 := make(chan interface{}, 1)
	quitChans[mr.IdString] = quitChan
	quitChans[mr2.IdString] = quitChan2

	got := string(store.Prune())
	if len(store.List) != 1 {
		t.Error("Pruning failed")
	}

	if len(quitChan) != 1 {
		t.Error("No quit message sent!")
	}

	if len(quitChan2) != 0 {
		t.Error("Quit message send to the wrong MR")
	}

	expected := "1 merge request was pruned\n"
	if got != expected {
		t.Errorf("Expected %s, got %s", expected, got)
	}
}

func tempConfig(t *testing.T) config.Config {
	dir, err := ioutil.TempDir("", "babysit_test")
	if err != nil {
		t.Error(err)
	}

	conf, err = config.NewConfig(dir)
	if err != nil {
		t.Error(err)
	}

	return conf
}
