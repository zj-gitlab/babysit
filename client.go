package main

import (
	"fmt"
	"os"

	"github.com/olekukonko/tablewriter"
)

func track(idString string) {
	cmd := Command{"track", map[string]string{"id": idString}}

	if response, err := cmd.sendCommand(); err != nil {
		fmt.Println(err)
	} else {
		fmt.Println(string(response))
	}
}

func list() {
	Load(&storage)

	table := tablewriter.NewWriter(os.Stdout)
	table.SetHeader([]string{"Merge Request", "Title", "Status"})

	for _, mr := range storage.List {
		table.Append(mr.ToTableRow())
	}

	table.Render()
}

func prune() error {
	cmd := Command{"prune", map[string]string{}}

	response, err := cmd.sendCommand()
	if err != nil {
		return err
	}

	_, err = fmt.Print(string(response))
	return err
}
