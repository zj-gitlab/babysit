package main

import "testing"

func TestMergeRequestConstructor(t *testing.T) {
	id := "gitlab-org/gitlab-ce!5555"
	mr := NewMergeRequest(id)

	if mr.IdString != id {
		t.Error()
	}

	if mr.ID != 5555 {
		t.Errorf("Error setting the IID, expect 5555, got %v", mr.ID)
	}
}

func TestMergeRequestStatus(t *testing.T) {
	cases := []struct {
		mr     MergeRequest
		status string
	}{
		{MergeRequest{State: "merged"}, "Merged"},
		{MergeRequest{State: "closed"}, "Closed"},
		{MergeRequest{WIP: true}, "Work in progress"},
		{MergeRequest{MergeStatus: "cannot_be_merged"}, "Merge conflicts"},
		{MergeRequest{AutoMerge: true}, "Auto merge enabled"},
		{MergeRequest{}, ""},
	}

	for _, c := range cases {
		got := c.mr.Status()

		if got != c.status {
			t.Errorf("Merge request status is: %q got: %q", got, c.status)
		}
	}

}

func TestProcessUpdate(t *testing.T) {
	storage = NewStorage()
	mr := MergeRequest{IdString: "test"}
	updated := MergeRequest{WIP: true}

	mr.processUpdate(updated)

	if !mr.WIP || mr.IdString != "test" {
		t.Error()
	}
}
