# Babysit

Developing on multiple MRs at the same time is painfull at times, especially
when you've got pipelines with high wall clock times. Babysit tries to make this
easier, for now by just requesting the status at an interval. Check the todo
list for what I hope to implement the next few days, weeks.

### How to

Installation is done through `go install`. I've compiled this locally with go1.7
and given I don't have CI setup just yet, so no support for other versions yet.
But its on the todo list, so this soon will be improved.

Babysit is meant to run as a backgroup service, or, as I expect it to be
unstable right now, just keep it running in a terminal. `babysit server` will do
the trick.

Than, in another terminal you can given the command to track a merge request,
this will use the GitLab shorthand. For example: `babysit track
'gitlab-org/gitlab-ce!5555'` starts tracking the merge request. You could list
as many as you want. Querying the state of them all is done through
`babysit list`.

`babysit --help` is your friend too.

### F.A.Q.

#### My access token is invalid?

By default, the config file is located at ~/.babysit.toml, in there you can set
your access token. Create one
[on your profile page](https://gitlab.com/profile/personal_access_tokens), and
fill it in there. An access token would work too, babysit only needs access to
the API it, right now. This might break babysit in the future though.

#### The `!` is rejected as input symbol?

No, its not. :) Bash and Zsh would like to to escape it. So either use
`track 'ns/project!12'` or `track ns/project\!12`.

#### Semver?

Yes! So I'm free to do whatever I'd like with the API until I release 1.0.
Given I haven't had much experience with CLI design, expect this to happen.
