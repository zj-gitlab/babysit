package config

import (
	"fmt"
	"io/ioutil"
	"os"
	"testing"
)

func TestDefaultConfigCreation(t *testing.T) {
	dir, err := ioutil.TempDir("", "babysit_test")
	if err != nil {
		t.Error(err)
	}
	defer os.RemoveAll(dir)

	_, err = NewConfig(dir)
	if err != nil {
		t.Fatal(err)
	}

	configFile := fmt.Sprintf("%s/config.toml", dir)
	// It writes a config file as non existed yet
	if _, err = os.Stat(configFile); err != nil {
		t.Error(err)
	}

	dat, err := ioutil.ReadFile(configFile)
	if err != nil {
		t.Error(err)
	}
	if string(dat) == "" {
		t.Error("Not data writen to config file")
	}
}
