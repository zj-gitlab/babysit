package config

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"os"
	"strings"

	"github.com/BurntSushi/toml"
)

const invalidToken = "invalid-token"

type Config struct {
	GitlabInstances []GitlabInstance
	configPath      string
}

type GitlabInstance struct {
	Endpoint    string `toml:"endpoint"`
	AccessToken string `toml:"access_token"`
}

// Lets use a hash soonish. Right now with a single instance O(N) == O(1)
func (c *Config) TokenForInstance(endpoint string) string {
	for _, instance := range c.GitlabInstances {
		if strings.Index(instance.Endpoint, endpoint) != -1 {
			return instance.AccessToken
		}
	}

	return invalidToken
}

func NewConfig(path string) (Config, error) {
	var config Config
	if path == "" {
		path = DefaultConfigPath()
	}
	config.configPath = path

	if _, err := os.Stat(config.ConfigFile()); os.IsNotExist(err) {
		sample := newGitLabInstance(invalidToken)
		config.GitlabInstances = []GitlabInstance{sample}
		err := config.writeDefaultConfig()

		if err != nil {
			return config, err
		}

		fmt.Printf("No config file found, wrote a sample for you in %s\n", path)
	}

	// No support for other instances than .com yet, but should be done in the future
	if _, err := toml.DecodeFile(config.ConfigFile(), &config); err != nil {
		return config, err
	}

	return config, nil
}

func (c *Config) StorageFile() string {
	return c.configPath + "/store.json"
}

func (c *Config) ConfigFile() string {
	return c.configPath + "/config.toml"
}

// Don't want to export it, as I don't want to see it in the TOML
func (c *Config) Path() string {
	return c.configPath
}

func DefaultConfigPath() string {
	return fmt.Sprintf("%s/.babysit/", os.Getenv("HOME"))
}

func (c *Config) writeDefaultConfig() error {
	buff := new(bytes.Buffer)
	if err := toml.NewEncoder(buff).Encode(c); err != nil {
		return err
	}

	return ioutil.WriteFile(c.ConfigFile(), buff.Bytes(), 0600)
}

func newGitLabInstance(token string) GitlabInstance {
	return GitlabInstance{Endpoint: "https://gitlab.com", AccessToken: token}
}
