package main

import (
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"strconv"
	"strings"
	"time"
)

type MergeRequest struct {
	// Withing GitLab, the ID is the primairy key
	ID          int    `json:"iid"`
	Title       string `json:"title"`
	ProjectID   int    `json:"project_id"`
	WIP         bool   `json:"work_in_progress"`
	MergeStatus string `json:"merge_status"`
	AutoMerge   bool   `json:"merge_when_build_succeeds`
	State       string `json:"state"`
	IdString    string
}

var httpClient = &http.Client{
	Timeout: 10 * time.Second,
	Transport: &http.Transport{
		MaxIdleConns:    5,
		IdleConnTimeout: 11 * time.Second,
	},
}

func NewMergeRequest(idString string) MergeRequest {
	mr := MergeRequest{IdString: idString}
	mr.setID()

	return mr
}

func (mr *MergeRequest) ToTableRow() []string {
	return []string{mr.IdString, mr.Title, mr.Status()}
}

func (mr *MergeRequest) Status() string {
	if mr.IsDone() {
		return strings.Title(mr.State)
	}

	if mr.WIP {
		return "Work in progress"
	} else if mr.MergeStatus == "cannot_be_merged" {
		return "Merge conflicts"
	} else if mr.AutoMerge {
		return "Auto merge enabled"
	} else {
		return ""
	}
}

func (mr *MergeRequest) IsDone() bool {
	return mr.State == "merged" || mr.State == "closed"
}

func (mr *MergeRequest) setID() {
	iid := strings.Split(mr.IdString, "!")[1]
	mr.ID, _ = strconv.Atoi(iid)
}

func (mr *MergeRequest) track() {
	quitChan := make(chan interface{})
	quitChans[mr.IdString] = quitChan

	mr.update()

	for {
		select {
		case <-time.After(7 * time.Minute):
			mr.update()
		case <-quitChan:
			return
		}
	}
}

func (mr *MergeRequest) update() {
	buff, err := mr.resourceReq()
	if err != nil {
		fmt.Println(err)
		return
	}

	var updated MergeRequest
	if err = json.Unmarshal(buff, &updated); err != nil {
		fmt.Println(err)
		return
	}
	mr.processUpdate(updated)

	fmt.Printf("Updated MR %s\n", mr.IdString)
}

func (mr *MergeRequest) processUpdate(updated MergeRequest) {
	updated.IdString = mr.IdString
	*mr = *&updated
	storage.Store(*mr)
}

func (mr *MergeRequest) resourceReq() ([]byte, error) {
	req, err := http.NewRequest("GET", mr.resourceURL(), nil)
	if err != nil {
		return nil, err
	}

	req.Header.Add("PRIVATE-TOKEN", conf.TokenForInstance("https://gitlab.com"))
	resp, err := httpClient.Do(req)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	if resp.StatusCode == 401 {
		return make([]byte, 0), errors.New("401 Unauthorized - Check your access token!")
	}

	return ioutil.ReadAll(resp.Body)
}

func (mr *MergeRequest) indexURL() string {
	return fmt.Sprintf("https://gitlab.com/api/v4/projects/%v/merge_requests", mr.projectId())
}

func (mr *MergeRequest) resourceURL() string {
	return fmt.Sprintf("%v/%v", mr.indexURL(), mr.ID)
}

func (mr *MergeRequest) projectId() string {
	nsProj := strings.Split(mr.IdString, "!")[0]
	nsProj = strings.Replace(nsProj, "/", "%2f", -1)

	return nsProj
}
