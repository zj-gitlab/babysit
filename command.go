package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net"
)

type Command struct {
	Cmd  string            `json:"command"`
	Args map[string]string `json:"arguments"`
}

func (cmd *Command) ToJSON() (string, error) {
	jsonString, err := json.Marshal(cmd)
	if err != nil {
		return "", err
	}

	return string(jsonString), nil
}

func (cmd *Command) sendCommand() ([]byte, error) {
	jsonBody, err := cmd.ToJSON()
	if err != nil {
		return nil, err
	}

	conn, err := net.Dial("tcp", serverPort)
	if err != nil {
		return nil, err
	}
	fmt.Fprintf(conn, jsonBody+"\n")

	message, _ := bufio.NewReader(conn).ReadBytes('\n')
	return message, nil
}
